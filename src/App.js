import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import Content from './components/content.js';

import { Button, Navbar, Nav, Form , FormControl, NavDropdown } from 'react-bootstrap';
class App extends Component{


  constructor(props) {
    super(props);
    this.idx= "";
    this.variant= "";}
  
  render() {
    return (
     <div className='App'>
       <Button variant="danger">Danger</Button><br/>
       List contents
       <div>
       <Navbar bg="light" expand="lg">
       <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
       <Navbar.Toggle aria-controls="basic-navbar-nav" />
       <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
      <Nav.Link href="#home">Home</Nav.Link>
      <Nav.Link href="#link">Link</Nav.Link>
      <NavDropdown title="Dropdown" id="basic-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
        </NavDropdown>
        </Nav>
        <Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        <Button variant="outline-success">Search</Button>
        </Form>
        </Navbar.Collapse>
        </Navbar>
          
       </div>
    </div>
  )
}}


export default App;
